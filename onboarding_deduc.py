#!/usr/bin/env python
# coding: utf-8

# In[1]:


#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from pandas.io import sql, gbq
import time, os
import datetime as dt
import mysql.connector as sqlcon
import os
import sys
import inspect
import psycopg2 as pg
import json
import mysql
import sqlalchemy
import pandas as pd
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint

# root_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

root_path = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.append(root_path)
from utility_scripts import gmail_utility
from utility_scripts import formatting
from config import db_config as db


RMS = db.credentials['RMS']
hl = db.credentials['hl_read']
frodo = db.credentials['frodo']
cps = db.credentials['CPS']
rissues = db.credentials['frodo_issues']
# ecom = db.credentials['Ecom']


cnx_rms = sqlcon.connect(host=RMS['host'], user=RMS['user'], 
                         password=RMS['password'], database=RMS['database'])
cnx2 = pg.connect(host = 'smaug-rep1.prod.db.shadowfax.in' , 
               user = 'ananya.shankar' ,password = 'htYsqhJ' ,  database = 'smaugdb')
cnx_hl = sqlcon.connect(host=hl['host'], user=hl['user'], 
                         password=hl['password'], database=hl['database'])
cnx_frodo = pg.connect(host = frodo['host'], user = frodo['user'],
                       password = frodo['password'], database=frodo['database'])
cnx_cps = sqlcon.connect(host=cps['host'], user=cps['user'], 
                         password=cps['password'], database=cps['database'])
cnx_rissues = pg.connect(host=rissues['host'], user=rissues['user'], 
                         password=rissues['password'], database=rissues['database'])
cnx_swiggy = mysql.connector.connect(user='shadowfax', password='shadowfax123',
                         database= 'swiggy', host='test.prod.db.shadowfax.in')




# In[2]:


query0 = '''select 
  distinct rider_id, 
  first_order_date
  from 
   first_order_date c where date(first_order_date) between date(LAST_DAY(NOW())-interval 30 day) and date(now()) '''

swiggy_df= sql.read_sql(query0,cnx_swiggy)


# In[3]:


swiggy_df


# In[4]:


query1 = '''select sfx_rider_id 
from RMS_riderinfo
where client_id<>2 '''

rms_df = sql.read_sql(query1,cnx_rms)


# In[5]:


rms_df


# In[6]:


swiggy_df.dtypes


# In[7]:


rms_df.dtypes


# In[8]:


swiggy_df['rider_id']=pd.to_numeric(swiggy_df['rider_id'],errors ='coerce')
rms_df['sfx_rider_id']=pd.to_numeric(rms_df['sfx_rider_id'],errors ='coerce')


# In[9]:


swiggy_df.dtypes
rms_df.dtypes


# In[10]:


non_ecomm_one_df=pd.merge(swiggy_df,rms_df,left_on='rider_id',right_on='sfx_rider_id',how='left')


# In[11]:


non_ecomm_one_df


# In[12]:


non_ecomm_one_df.drop_duplicates(subset=None)


# In[ ]:





# In[13]:


non_ecomm_one_df=non_ecomm_one_df.groupby(['first_order_date']).agg({'sfx_rider_id':'count'}).reset_index()


# In[14]:


non_ecomm_one_df.rename(columns={'sfx_rider_id':'one_order_count'},inplace=True)


# In[15]:


query3= '''select rider_id, date fifty_order_date from fifty_order_date 
where date between date(LAST_DAY(NOW())-interval 30 day) and date(now())'''


# In[16]:


fifty_df=sql.read_sql(query3,cnx_swiggy)


# In[17]:


non_ecomm_fifty_df=pd.merge(fifty_df,rms_df,left_on='rider_id',right_on='sfx_rider_id',how='left')


# In[18]:


non_ecomm_fifty_df


# In[19]:


non_ecomm_fifty_df.drop_duplicates(subset=None)


# In[20]:


non_ecomm_fifty_df=non_ecomm_fifty_df.groupby(['fifty_order_date']).agg({'rider_id':'count'}).reset_index()


# In[21]:


non_ecomm_fifty_df


# In[22]:


non_ecomm_fifty_df.rename(columns={'rider_id':'fifty_order_count'},inplace=True)


# In[23]:


joined=pd.merge(non_ecomm_one_df,non_ecomm_fifty_df,left_on='first_order_date',right_on='fifty_order_date',how='outer')


# In[24]:


joined


# In[25]:


joined.drop(['fifty_order_date'],axis=1,inplace=True)


# In[26]:


joined.rename(columns={'sfx_rider_id':'one_order_count','rider_id':'fifty_order_count','first_order_date':'date'},inplace=True)


# In[27]:


non_ecom=joined


# In[28]:


query4 = '''select sfx_rider_id 
from RMS_riderinfo
where client_id=2 '''

rms_df2 = sql.read_sql(query4,cnx_rms)


# In[29]:


swiggy_df['rider_id']=pd.to_numeric(swiggy_df['rider_id'],errors ='coerce')
rms_df2['sfx_rider_id']=pd.to_numeric(rms_df['sfx_rider_id'],errors ='coerce')


# In[30]:


ecomm_one_df=pd.merge(swiggy_df,rms_df2,left_on='rider_id',right_on='sfx_rider_id',how='left')


# In[31]:


ecomm_one_df.drop_duplicates(subset=None)


# In[32]:


ecomm_one_df.drop(['sfx_rider_id'],axis=1,inplace=True)


# In[33]:


ecomm_one_df


# In[34]:


ecomm_one_df=ecomm_one_df.groupby(['first_order_date']).agg({'rider_id':'count'}).reset_index()


# In[35]:


ecomm_one_df.rename(columns={'rider_id':'one_order_count','first_order_date':'date'},inplace=True)


# In[36]:


ecom=ecomm_one_df


# In[37]:


ecom


# In[38]:


from datetime import datetime

given_date = datetime.today().date()

first_day_of_month = given_date.replace(day=1)


# In[ ]:





# In[39]:


query6='''select distinct(date(date_raised)),description,
  sum(amount*count(distinct rider_id)) over (partition by description,date(created)),count(rider_id) over (partition by description,date(created)) as rider_count
  from slot_payouts_partnerdeduction 
  where 
  category in (156,157,289,290) 
  and date(created) between '{first_day_of_month}' and '{given_date}'
  group by 1,amount,rider_id,description,created
  order by 1 desc'''.format(first_day_of_month=first_day_of_month,given_date=given_date)

pay_df=sql.read_sql(query6,cnx2)


# In[40]:


pay_df


# In[41]:


# hl_riders = pay_df.loc[(pay_df.description == 'HL Rider First Order Onboarding Deduction') & (pay_df.description =='HL Rider Fifty Order Onboarding Deduction')]
hl_riders = pay_df[(pay_df['description'].isin(['HL Rider First Order Onboarding Deduction', 'HL Rider Fifty Order Onboarding Deduction']))]
ecom_riders = pay_df.loc[pay_df.description== 'ECOM Rider First Order Onboarding Deduction']


# In[42]:


hl_riders


# In[43]:


ecom_riders


# In[44]:


# index_names = df[(df['lead_source_id'] == 1) & (df['referring_partner_role'].isin(['Ops', 'Freelance BD', 'Staffing Partner', 'Central-BD']))].index
# df.drop(index_names, inplace = True) 

# df
# df12=df.loc[(df['first_upload_time'] == date1) & (df['last_interest_time']==0)]


# In[45]:


hl_riders['date'] = pd.to_datetime(hl_riders['date'])
hl_riders


# In[46]:


hl_pay = pd.DataFrame([hl_riders.groupby(pd.Grouper(freq='D', key='date'))['sum'].sum()]).transpose().reset_index()
hl_pay


# In[47]:


ecom_pay=ecom_riders[['date','sum']]


# In[48]:


ecom_pay


# In[49]:


hl_pay


# In[50]:


non_ecom


# In[51]:


final_necom=pd.merge(hl_pay,non_ecom,on='date',how='outer')


# In[52]:


final_necom


# In[53]:


# final_necom.drop.loc[]('date',axis=0)
# final_necom.drop(final_necom[(final_necom['date']).min()].index)
# between_two_dates = first_day_of_month , given_date
# final_necom = final_necom.loc[between_two_dates]
# final_necom.drop(pd.to_datetime(final_necom['date']).min(),axis=0)


# In[54]:


final_necom['had_to_be_deducted']=500*(final_necom['one_order_count']+final_necom['fifty_order_count'])
final_necom.rename(columns={'sum':'deducted_on_that_day'},inplace=True)
final_necom.fillna(0,inplace=True)
final_necom[(final_necom['date']>=pd.to_datetime(first_day_of_month))]


# In[55]:


final_necom


# In[56]:


ecom['date']=pd.to_datetime(ecom['date'])
ecom_pay['date']=pd.to_datetime(ecom_pay['date'])


# In[57]:


final_ecom=pd.merge(ecom_pay,ecom,on='date',how='outer')
final_ecom['had_to_be_deducted']=500*(final_ecom['one_order_count'])
final_ecom.rename(columns={'sum':'deducted_on_that_day'},inplace=True)
final_ecom.fillna(0,inplace=True)
final_ecom[(final_ecom['date']>=pd.to_datetime(first_day_of_month))]
final_ecom.loc['Column_Total']= final_ecom.sum(numeric_only=True, axis=0)


# In[58]:


final_ecom
order = [0,2,1,3] # setting column's order
final_ecom= final_ecom[[final_ecom.columns[i] for i in order]]
final_ecom


# In[59]:


final_necom
order = [0,2,3,1,4] # setting column's order
final_necom= final_necom[[final_necom.columns[i] for i in order]]


# In[60]:


final_necom
final_necom.loc['Column_Total']= final_necom.sum(numeric_only=True, axis=0)
final_necom


# In[61]:

writer = pd.ExcelWriter('Non_Ecomm_Onboarding_deduc_day_wise_MTD.xlsx')
final_necom.to_excel(writer, sheet_name= 'Non_Ecomm_Onboarding_deduc')
writer.save()
d = dt.date.today()

writer = pd.ExcelWriter('Ecom_Onboarding_deduc_day_wise_MTD.xlsx')
final_necom.to_excel(writer, sheet_name= 'Ecom_Onboarding_deduc')
writer.save()
d = dt.date.today()


# In[ ]:


attachment_list = ['Non_Ecomm_Onboarding_deduc_day_wise_MTD.xlsx','Ecom_Onboarding_deduc_day_wise_MTD.xlsx']
gmail_utility.send_email(['stuti.singhal@shadowfax.in'])
    mimetype_parts_dict={'html': "Hi, <br><br>" +                                  "Please find onboarding deduction file.<br><br>"+                                  "<b><u> Overall Hiring Report: "  + " </u></b><br><br>" +                                  formatting.apply_generic_css2(final.to_html(index=True)) +
                                 "<br><br>"
}, attach_multi=attachment_list, from_user='reports')


# In[ ]:




